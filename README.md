# debtips

This is a personal blog-like repository where I document things I've learned about debian packaging.

It contains cheatsheets for every tool I use.  My basic rule is:

> If I need to search the internet for an answer, add it here.
> If I have ever had problems finding it in a man-page, add it here.

It's mostly a reference for myself.  If anyone else finds anything here useful, cool.

