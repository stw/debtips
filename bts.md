# Debian Bug Tracking System

This guide uses `foo` as a source name, `foo` as a package name, and `nnn` as a bug number.

Relevant URLs are at:

- https://bugs.debian.org
- https://www.debian.org/Bugs/
- https://udd.debian.org/
- https://www.debian.org/Bugs/server-control

## Creating bugs:

Email `submit@bugs.debian.org`. The subject will be the bug's title.

The body should be:

```
Package: foo
Version: 1.2.3-4
Source: foo
Severity: wishlist
Tags: newcomer security

Bug details here
```

Only include Pseudo-header lines that are actually relevant.  Generally, you should have *either* the package or the source defined, and the version.

See https://www.debian.org/Bugs/Reporting for details.

## Commenting:

To comment on a bug, email `nnn@bugs.debian.org` with the comment.  You may also CC `control@bugs.debian.org` and prepend any [control commands](https://www.debian.org/Bugs/server-control) followed by `thank you` to the same email.

## Change ownership

When working on a bug, it's good to claim ownership to avoid having your toes stepped on. To do that, send an email to `control@bugs.debian.org` with the following message body:

```
owner nnn !
thank you
```

The `!` means "me", but you could also put someone's email there to give ownership.

To give up ownership send: 
```
noowner nnn
thank you
```

## Tagging a bug:

Send an email to `control@bugs.debian.org` with

```
tags nnn + pending
thank you
```

Tags are documented here: https://www.debian.org/Bugs/Developer#tags

## Usertags:

Send an email to `control@bugs.debian.org`.  If there is more info than just a pseudo-header then add `nnn@bugs.debian.org` so that your comments are attached to the bug.

This is the email I used to add a bsp usertag to bugs:
```
user debian-release@lists.debian.org
usertags nnn + bsp-2019-01-nl-venlo
thank you
```

## Closing a bug: 

Usually, just add `(Closes: nnn)` to your `debian/changelog`.  When the package is accepted into unstable, the bug will be closed. 

To close a bug without a change, send an email to `nnn-done@bugs.debian.org` with the following info: 

```
Source: foo
Version: 1.2.3-4

Some text describing how it was closed.
```

## Intent to package (ITP)

If you want to contribute a new package, file an ITP.

https://www.debian.org/devel/wnpp/#l2
