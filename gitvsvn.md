
# Git Versus SVN

Git and SVN counterpart commands: 

| git                          | svn                                    |
|------------------------------|----------------------------------------|
| `git clone <url>`            | `svn checkout <url>`                   |
| `git pull`                   | `svn update`                           |
| `git add <newfile>`          | `svn add <newfile>`                    |
| `git rm <deletedfile>`       | `svn rm <deletedfile>`                 |
| `git mv <movedfile>`         | `svn mv <movedfile>`                   |
| `git add <modifiedfile>`     | *automatic*                            |
| `git reset <stagedfile>`     | *n/a*                                  |
| `git status`                 | `svn status`                           |
| `git diff`                   | `svn diff`                             |
| `git diff --staged`          | `svn diff`                             |
| `git diff <sha1>`            | `svn diff -c REV`                      |
| `git blame <file>`           | `svn blame <file>`                     |
| `git format-patch -1 <sha1>` | `svn diff -c REV > file.patch`         |
| `git am < file.patch`        | `svn patch file.patch`                 |
| `git checkout <file>`        | `svn revert <file>`                    |
| `git checkout <sha1>`        | `svn update -r REV`                    |
| `git checkout <branch>`      | `svn switch ^/branches/<branch>`       |
| `git commit && git push`     | `svn commit`                           |
| `git fetch --all && git log` | `svn log`                              |
| `git branch -c <newbranch>`  | `svn cp . ^/branches/<newbranch>`      |
| `git branch -d <oldbranch>`  | `svn rm ^/branches/<oldbranch>`        |
| `git tag <newtag>`           | `svn cp . ^/tags/<newtag>`             |
| `git merge <featurebranch>`  | `svn merge ^/branches/<featurebranch>` |
| `git init`                   | `svnadmin create /repo && svnserve -d && svn co svn://localhost/repo` |
| `git remote set-url origin <url>` | `svn relocate <url>`              |
| `git clean -df .`            | `svn cleanup --remove-unversioned`     |
| `git range-diff REV1...REV2` | `svn diff -r REV1:REV2`                |
| `git revert <sha1>`          | `svn merge -r BADREV:GOODREV .`        |



Extra SVN commands:

- `auth`
- `cat`, `list`, `mkdir`
- `changelist`
- `export`, `import`
- `info`
- `lock`, `unlock`
- `mergeinfo`
- `prop{del|edit|get|list|set}`
- `resolve`, `resolved`
- `upgrade`
- `shelve`, `unshelve`, `shelves`

Extra git commands:

- `archive`, `bundle`, `describe`, `notes`, `show`
- `bisect`
- `cherry-pick`
- `citool`, `gitk`, `gui`
- `gc`
- `grep`
- `rebase`
- `shortlog`
- `stash`
- `submodules`
- `worktree`

## Centralization

Subversion is centralized while git is decentralized.  If you've even heard of git and subversion, then you already knew that.  The rest of this section describes what that means and analyzes the pros and cons of each.

Subversion requires a remote repository in order to function.  While git can run totally stand-alone. A developer can `git init` and start working by himself. That's cool for sandboxing and learning git, but not so useful for real development where you actually need to collaborate. To use git in a more useful way, you'll want to add a *remote* to your git repository to make it work similarly to subversion and help you upload your changes to your team.

Subversion can only handle one remote repository at a time.  It can be changed with `svn relocate`, but you can only have one at a time.  Git, on the otherhand, can handle multiple repositories.  It can pull branches from both repositories and push changes from one to another.

Let's say your git repository has two remotes: `upstream` and `downstream`.  This will help you pull changes from a branch upstream and push them downstream:

```
git remote set-url upstream <upstreamurl>     # Set remotes
git remote set-url downstream <downstreamurl>
git branch --track up upstream/master         # Track master branches in both remotes
git branch --track down downstream/master
git fetch --all                               # Get all information
git pull --all
git checkout down                             # Work on downstream
git merge up                                  # Merge upstream changes to downstream
git push downstream                           # Push the upstream changes downstream
```

Because all subversion commits occur on the same repository (not on other remotes or in your local repository) we gain the ability to synchronize.  By that, I mean we can have sequential numbering of commits.  If the bug tracker says that a bug was reported in revision 5413, fixed in rev 5537, merged to trunk in rev 5583, released in rev 5619, and I'm testing rev 5620, then I know that I can see if the bug is fixed. There is a clear timeline here which just can't exist in git.  If my bug was report in `558c44492a491402752ca59bb3c30816fdb18a64`, fixed in `8148591822f9f03c2827ef652973511155c1530f`, and I'm testing `3dce6ee8d7515dc6d71a8c649c91c977e9cde691`, then I really need to consult some type of timeline to figure out whether I should be testing this change.

**+1 to svn**

## Staging

Git adds a feature called "staging".  This is one of the most prominent features of git, and one that I love.  It let's you flag specific changes for commit, long before you actually make the commit. Even if you change a file after it has been staged, only the staged changes will actually be commited and you'll be able to view both the staged and unstaged changes to that file.  This helps the dev to spend more time reviewing changes to ensure each commit is right before committing it. That means fewer bad or incomplete commits.

**+1 to git**

## Merging

Let's say that we merge a feature branch to master/trunk. Let's say that the feature branch has 20 commits. After the merge, we'll see 20 new commits added to the master branch.  In subversion, we'll see 1 new commit (concatination of all commits) added to the master branch.

Which is better?  One may argue that more information is always better, but often it's just noise. Do we really need to see every private save-point made by a developer on a personal branch? If the answer is yes, then is it reasonable that it's hidden by default and that we'd need to dig a little deeper by inspecting the feature branch directly?  If the answer is yes, then **+1 to svn**.  If the answer is no, then **+1 to git**.

Furthermore, is the person merging the feature to trunk the same as the author of the commits?  If not, then your logfile is going to show the integrator as the author instead of the actual developer.  That's misleading at best.  **-1 to svn**.
