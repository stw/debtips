# Lintian

Lintian is used to statically analyze a source.  In debian we use it to check a package. 

To view everything you could ever want to know: 

```
lintian -i -E -I --pedantic --color always ../*.changes
```
