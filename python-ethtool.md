Note: 

python-ethtool is already available in official repositories.  

Debian tried to package python3-ethtool with version 0.12, but python3 support was too buggy. Python3 is now fully supported as of version 0.13 so I am packaging it. 

Debian's scripts are here: 
https://sources.debian.org/src/python-ethtool/0.12-1.2/

They are very similar to ours (I'm impressed!).

## Get the original source
```
wget https://github.com/fedora-python/python-ethtool/archive/v0.14.tar.gz
mv v0.14.tar.gz python-ethtool-0.14.tar.gz
```

## Build it (this can be run as a script easily)
```
# Exit if we get errors
set -e

# Clean up from previous run if this isn't the first time we are building
cd ~/src 
rm -rf python-ethtool-0.14
rm -f python-ethtool_*
rm -f python3-ethtool_*

# Unpack the archive
tar -xzf python-ethtool-0.14.tar.gz
cd python-ethtool-0.14

# Generate the build instruction templates
DEBNAME="Stewart Ferguson"
dh_make --email stew@sim-international.com --copyright=gpl -i --file=../python-ethtool-0.14.tar.gz --yes

# Fill in the details
cp ~/src/wc/packages/python3-ethtool/debian/control debian/control
cp ~/src/wc/packages/python3-ethtool/debian/rules debian/rules

# Build and package
fakeroot dpkg-buildpackage -us -uc
```
The packages are now all in `~/src`