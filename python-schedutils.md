## Get the original source
```
wget https://git.kernel.org/pub/scm/libs/python/python-schedutils/python-schedutils.git/snapshot/python-schedutils-0.6.tar.gz
```

## Build it (this can be run as a script easily)
```
# Exit if we get errors
set -e

# Clean up from previous run if this isn't the first time we are building
cd ~/src 
rm -rf python-schedutils-0.6
rm -f python-schedutils_*
rm -f python3-schedutils_*

# Unpack the archive
tar -xzf python-schedutils-0.6.tar.gz
cd python-schedutils-0.6

# Generate the build instruction templates
DEBNAME="Stewart Ferguson"
dh_make --email stew@ferg.aero --copyright=gpl -i --file=../python-schedutils-0.6.tar.gz --yes -p python-schedutils_0.6

# Fill in the details
cp ~/src/wc/packages/python3-schedutils/debian/control debian/control
cp ~/src/wc/packages/python3-schedutils/debian/rules debian/rules

# Build and package
fakeroot dpkg-buildpackage -us -uc
```
The packages are now all in `~/src`
