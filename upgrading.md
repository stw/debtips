# Upgrade procedure

## 1. Aquire previous source

Clone

```
git clone git@salsa.debian.org:python-team/modules/foo.git
cd foo
```

Track `master`, `upstream`, and `pristine-tar`

```
git branch --track master origin/master
git branch --track upstream origin/upstream
git branch --track pristine-tar origin/pristine-tar
git fetch --all
git pull --all
```

## 2. Get new foo_x.y.orig.tar.gz

If watch file works:

```
uscan
```

Otherwise, just download it manually

```
wget https://github.com/user/foo/archive/v1.9.0.tar.gz -O ../foo_1.9.0.orig.tar.gz
```

## 3. Prepare the upstream branch

```
git checkout `upstream`
rm -rf *

tar -xvf ../foo_1.9.0.orig.tar.gz --strip-components=1

git add -A
git commit -m "Imported Upstream version 1.9.0"
git tag upstream/1.9.0
```

## 4. Prepare the `pristine-tar` branch

This script brings data from the tarball and the `upstream` branch and creates new files in the `pristine-tar` branch
```
pristine-tar commit ../foo_1.9.0.orig.tar.gz
```

Next, check that the `pristine-tar`-generated file is really the same as the original tarball:
```
pristine-tar checkout foo_1.9.0.orig.tar.gz
md5sum foo_1.9.0.orig.tar.gz ../foo_1.9.0.orig.tar.gz
rm foo_1.9.0.orig.tar.gz
```

## 4. Merge the `upstream` changes to `master`

```
git checkout master
git merge upstream
```

## 5. Make changes to `debian/*`

```
dch
dpkg-buildpackage -uc -us
lintian -i -L ">=pedantic" ../foo_1.9.0-1_amd64.changes
```

## 6. Push everything

```
git push origin --all
git push --tags
```

